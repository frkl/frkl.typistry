# -*- coding: utf-8 -*-
"""Top-level package for '*frkl.typistry*'."""

__all__ = [
    "__author__",
    "__email__",
    "get_version",
    "Typistry",
]

import logging
import os

from frkl.typistry.typistry import Typistry

log = logging.getLogger("frkl_typistry")

__author__ = """Markus Binsteiner"""
"""The (main) author of '*frkl.typistry*'."""
__email__ = "markus@frkl.io"
"""The email address of the (main) author of '*frkl.typistry*'."""


def get_version() -> str:
    """Return the current version of '*frkl.typistry*'.

    Returns:
        the version string, or 'unknown'
    """

    from pkg_resources import DistributionNotFound, get_distribution

    try:
        # Change here if project is renamed and does not equal the package name
        dist_name = __name__
        __version__ = get_distribution(dist_name).version
    except DistributionNotFound:

        try:
            version_file = os.path.join(os.path.dirname(__file__), "version.txt")

            if os.path.exists(version_file):
                with open(version_file, encoding="utf-8") as vf:
                    __version__ = vf.read()
            else:
                __version__ = "unknown"

        except (Exception):
            pass

        if __version__ is None:
            __version__ = "unknown"

    return __version__
