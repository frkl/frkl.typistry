# -*- coding: utf-8 -*-
"""The entry point for the commandline-interface for *frkl.typistry*."""
import os
from pathlib import Path
from typing import Any, Mapping

import asyncclick as click
from frkl.common.formats.auto import get_content_async
from frkl.tings.payload import TingInstancePayload

from frkl.tings.tingistry import Tingistry

from rich.traceback import install

from frkl.tings.utils import print_typistry_info, print_tingistry_info

install()

try:
    import uvloop

    uvloop.install()
except Exception:
    pass

click.anyio_backend = "asyncio"

# class SubclassTingFactory(TingFactory):
#
#     def create_ting_obj(self, ting_id: str, prototing: ProtoTing, ting_data: Any):
#
#         if not isinstance(ting_data, Mapping):
#             raise FrklException(msg="Can't create ting object.", reason=f"Data must be a mapping, not: {type(ting_data)}")
#
#         type_key = prototing.config.get("type_key", "type")
#         if type_key not in ting_data.keys():
#             raise FrklException(msg="Can't create ting object.",
#                                 reason=f"No '{type_key}' key available in ting data.")
#
#         base_type = prototing.tingistry.typistry.get_type(prototing.type_alias)
#         assert issubclass(base_type.cls, ConfigTing)
#         subtypes = prototing.tingistry.typistry.get_subtypes(prototing.type_alias)
#         subtype = ting_data[type_key]
#
#         if subtype not in subtypes.keys():
#             raise FrklException(msg="Can't create ting object.",
#                                 reason=f"No sub-type '{subtype}' available.", solution=f"Available subtypes: {', '.join(subtypes.keys())}")
#
#         subtype_cls = subtypes[subtype].cls
#         obj = subtype_cls(config=ting_data)
#         return obj

import frkl.tings.examples.project_types

@click.command()
@click.pass_context
async def cli(ctx):



    resources = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..", "..", "..", "..", "tests", "resources"))
    input_file = Path(resources) / "input.json"

    content = await get_content_async(input_file)

    prototing_config = {
        "type_key": "project_type"
    }

    tingistry_config = {
        "id": "default",
        "module_namespaces": ["frkl.tings.examples.*", "frkl.tings.payload", "frkl.tings.quality"],
        "tings": [
            {
                "ting_id": "project_type",
                "ting_config": prototing_config
            }
        ]
    }
    tingistry_config = {}

    tingistry = Tingistry.instance(tingistry=tingistry_config)
    tingistry._tingistry_init()
    tpl = TingInstancePayload(ting_type="python_project")
    tingistry.create_ting(ting_type="python_project", ting_id="project.python_project")

    print_tingistry_info(tingistry=tingistry)
    #
    # import pp
    # ting = tingistry.create_ting("project_type", "ting_1", ting_data=content)
    # print(ting)
    # print(ting.obj)
    # pp(ting.obj.config)

    # tt = TingistryThread.create_and_start(tingistry_config=tingistry_config)
    # tt.join()


if __name__ == "__main__":
    cli()
