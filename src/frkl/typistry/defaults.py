# -*- coding: utf-8 -*-
import os
import sys
from appdirs import AppDirs

frkl_typistry_app_dirs = AppDirs("frkl_typistry", "frkl")

if not hasattr(sys, "frozen"):
    FRKL_TYPISTRY_MODULE_BASE_FOLDER = os.path.dirname(__file__)
    """Marker to indicate the base folder for the `frkl_typistry` module."""
else:
    FRKL_TYPISTRY_MODULE_BASE_FOLDER = os.path.join(
        sys._MEIPASS, "frkl_typistry"  # type: ignore
    )
    """Marker to indicate the base folder for the `frkl_typistry` module."""

FRKL_TYPISTRY_RESOURCES_FOLDER = os.path.join(
    FRKL_TYPISTRY_MODULE_BASE_FOLDER, "resources"
)
"""Default resources folder for this package."""

TUI_HISTORY_FILE = os.path.join(frkl_typistry_app_dirs.user_data_dir, "tui_history.log")
"""The default tui history file path."""


DEFAULT_TYPELOADER_ID = "__default_typeloader__"
DEFAULT_TYPISTRY_ID = "__default_typistry__"
