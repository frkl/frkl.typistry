# -*- coding: utf-8 -*-
from typing import (
    Any,
    Callable,
    Dict,
    Iterable,
    Mapping,
    Optional,
    Set,
    Union,
)

from frkl.common.exceptions import FrklException
from frkl.common.strings import from_camel_case, generate_valid_identifier
from frkl.typistry.core import get_type_alias, RegisteredType
from frkl.typistry.defaults import DEFAULT_TYPISTRY_ID
from frkl.typistry.type_loader import TypeInput, TypeLoader


def default_type_alias_generator(r_type: RegisteredType):
    """Creates snake-case alias from whole class namespace string."""

    return ".".join((from_camel_case(token) for token in r_type.alias.split(".")))


def default_subclass_alias_generator(r_type: RegisteredType):

    type_alias = get_type_alias(r_type.cls)
    return from_camel_case(type_alias)


class Typistry(object):
    """Higher level class for managing types and their sub-types within a Python interpreter instance."""

    _instances: Dict[str, "Typistry"] = {}

    @classmethod
    def register_instance(cls, typistry: "Typistry"):

        if typistry.typistry_id in typistry._instances.keys():
            raise FrklException(msg="Can't register typestry.", reason=f"Typistry with id '{typistry.typistry_id}' already registered.")

        cls._instances[typistry.typistry_id] = typistry

    @classmethod
    def instance(cls, typistry: Union[None, str, "Typistry"]) -> "Typistry":

        if typistry is None:
            if DEFAULT_TYPISTRY_ID not in cls._instances.keys():
                _instance = Typistry(typistry_id=DEFAULT_TYPISTRY_ID, type_loader=TypeLoader.instance())
                cls.register_instance(_instance)
            return cls._instances[DEFAULT_TYPISTRY_ID]
        elif isinstance(typistry, str):
            if typistry not in cls._instances.keys():
                if not cls._instances:
                    if typistry == DEFAULT_TYPISTRY_ID:
                        _instance = Typistry(typistry_id=DEFAULT_TYPISTRY_ID, type_loader=TypeLoader.instance())
                        cls.register_instance(_instance)
                        return cls._instances[typistry]
                    else:
                        raise FrklException(msg=f"Can't retrieve typistry instance with id '{typistry}'.",
                                            reason="No typistries registered (yet).")
                else:
                    raise FrklException(msg=f"Can't retrieve typistry instance with id '{typistry}'.",
                                        reason="No typistry with this id registered.",
                                        solution=f"Registered typistry ids: {' '.join(cls._instances.keys())}.")
            return cls._instances[typistry]
        elif isinstance(typistry, Typistry):
            return typistry
        else:
            raise TypeError(f"Invalid type, not a Typistry: {type(typistry)}")

    def __init__(self, typistry_id: Optional[str]=None, type_loader: Union[None, str, TypeLoader] = None):

        if typistry_id is None:
            typistry_id = generate_valid_identifier(name="typistry_")

        self._typistry_id: str = typistry_id

        self._type_loader: TypeLoader = TypeLoader.instance(
            type_loader=type_loader
        )

        self._type_alias_map: Dict[str, RegisteredType] = {}
        self._subclass_map: Dict[str, Dict[str, RegisteredType]] = {}
        self._subclass_func_map: Dict[str, Callable] = {}
        self._objects: Dict[str, Dict[str, Any]] = {}

    @property
    def typistry_id(self) -> str:
        return self._typistry_id

    def register_module_namespaces(
        self, *module_namespaces: str, bundle_alias: Optional[str] = None
    ):
        invalidated_types = self._type_loader.register_module_namespaces(
            *module_namespaces, bundle_alias=bundle_alias
        )

        to_clear: Set[str] = set()
        for base_type, subclasses in self._subclass_map.items():
            base_r_type = self.get_type(base_type)
            if base_r_type in invalidated_types:
                to_clear.add(base_type)

        for r_type in to_clear:
            self._subclass_map.pop(r_type)

    @property
    def registered_type_aliases(self) -> Iterable[str]:
        return self._type_alias_map.keys()

    def register_type(
        self,
        cls: TypeInput,
        type_alias: Union[None, str, Callable] = None,
        allow_existing: bool = False,
    ) -> str:
        """Register a type in this typistry.

        Arguments:


        Returns:
            the type alias for the registered type.
        """

        if not isinstance(cls, RegisteredType):
            r_type = RegisteredType.create_type(cls)
        else:
            r_type = cls

        if not type_alias:
            type_alias = default_type_alias_generator(r_type)
        elif callable(type_alias):
            type_alias = type_alias(r_type)
        elif not isinstance(type_alias, str):
            raise FrklException(
                msg=f"Can't register type with type alias '{type_alias}'.",
                reason=f"Invalid type '{type(type_alias)}', must be either a string, or a callable.",
            )

        if type_alias in self._type_alias_map.keys():
            if not allow_existing:
                raise FrklException(
                    msg=f"Can't register type with alias '{type_alias}'.",
                    reason="A type with that name was already registered earlier.",
                )
            else:
                already_registered = self._type_alias_map[type_alias]
                if not already_registered == r_type:
                    raise FrklException(
                        msg=f"Can't register type with alias '{type_alias}'.",
                        reason="Another type is already registered under this name.",
                    )
                return self._type_alias_map[type_alias]

        self._type_alias_map[type_alias] = self._type_loader.register_type(
            cls=r_type, allow_existing=True
        )
        return type_alias

    @property
    def registered_types(self) -> Iterable[str]:
        return sorted(self._type_alias_map.keys())

    def get_type(self, type_alias: str) -> RegisteredType:

        if type_alias not in self._type_alias_map.keys():
            raise FrklException(msg=f"No type registered under alias '{type_alias}'.")

        return self._type_alias_map[type_alias]

    def get_registered_subtypes_for(self, base_type: str) -> Iterable[str]:
        """Return all registered subtypes for the specified base type."""

        return sorted(self.get_subtypes(base_type=base_type).keys())

    @property
    def registered_subtypes(self) -> Mapping[str, Iterable[str]]:

        result = {}
        for base_type in self._subclass_map.keys():
            result[base_type] = list(self.get_registered_subtypes_for(base_type=base_type))
        return result

    def get_subtypes(self, base_type: str) -> Mapping[str, RegisteredType]:

        if base_type in self._subclass_map.keys():
            return self._subclass_map[base_type]
        else:
            func = default_subclass_alias_generator
            if base_type in self._subclass_func_map.keys():
                func = self._subclass_func_map[base_type]
            return self.register_subtypes(base_type=base_type, type_alias_func=func)

    def get_subtype(self, base_type: str, type_alias: str) -> RegisteredType:

        subtypes = self.get_subtypes(base_type=base_type)
        if type_alias not in subtypes.keys():
            raise FrklException(msg=f"Can't retrieve subtype '{type_alias}' of base type '{base_type}'.", reason="Subtype not available.", solution=f"Use one of the available subtypes: {', '.join(subtypes.keys())}")

        return subtypes[type_alias]

    def register_subtypes(
        self,
        base_type: str,
        allow_existing: bool = False,
        type_alias_func: Optional[Callable] = None,
    ) -> Dict[str, RegisteredType]:

        r_type = self.get_type(base_type)

        if r_type.alias in self._subclass_map.keys():
            if allow_existing:
                return self._subclass_map[r_type.alias]

        if type_alias_func is None:
            type_alias_func = default_subclass_alias_generator

        subclass_map: Dict[str, RegisteredType] = {}
        subclasses: Mapping[
            str, RegisteredType
        ] = self._type_loader.register_subclasses(r_type)

        for sc_r_type in subclasses.values():

            name = type_alias_func(sc_r_type)
            full_type_alias = f"{base_type}.{name}"
            r_type_alias = self.register_type(
                cls=sc_r_type, type_alias=full_type_alias, allow_existing=False
            )
            subclass_map[name] = self.get_type(r_type_alias)

        self._subclass_map[base_type] = subclass_map
        self._subclass_func_map[base_type] = type_alias_func
        return self._subclass_map[base_type]

