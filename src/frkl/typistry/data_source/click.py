# -*- coding: utf-8 -*-
import typing
from asyncclick import Command
from pydantic import BaseModel

from frkl.typistry import Tyng, TyngConf
from frkl.typistry.data_source import DataSource
from frkl.typistry.tyngistry import Tyngistry


class ClickDataSource(DataSource):
    _type_alias = "cli"

    class ConfigSchema(TyngConf):
        pass

    def __init__(
        self, tyngistry: Tyngistry, config: typing.Mapping[str, typing.Any] = None
    ):

        super().__init__(config=config, tyngistry=tyngistry)

    def create_command(self) -> Command:

        # command = Command(name="")
        pass

    async def create_config(
        self, cls: typing.Union[typing.Type[BaseModel], typing.Type[Tyng]]
    ) -> typing.Dict[str, typing.Any]:

        subclasses = self.get_subclasses(cls)
        type_alias = self._tyngistry.typistry.get_type_alias(cls)

        print(subclasses)
        print(type_alias)

        # if len(subclasses) > 0:
        #     command = SubClassMultiCommand(type_alias=type_alias, tyngistry=self._tyngistry)
