# -*- coding: utf-8 -*-
import copy
import os
import typing
from prompt_toolkit import PromptSession
from prompt_toolkit.completion import Completer, Completion, WordCompleter
from prompt_toolkit.history import FileHistory
from pydantic import BaseModel, Field

from frkl.common.exceptions import FrklException
from frkl.typistry import Tyng, TyngConf, Typistry
from frkl.typistry.core import TYPE_MARKER_KEY, SubClassEnum, get_type_alias
from frkl.typistry.data_source import DataSource
from frkl.typistry.tyngistry import Tyngistry


class ProjectTypeCompleter(Completer):
    def __init__(self, typistry: typing.Optional[Typistry] = None):

        if typistry is None:
            typistry = Typistry.instance()
        self._typistry: Typistry = typistry

    def get_completions(self, document, complete_event):
        yield Completion("completion", start_position=0)


class TuiDataSource(DataSource):
    _type_alias = "tui"

    class ConfigSchema(TyngConf):
        history_file: typing.Optional[str] = Field(
            description="The history file, if any.", default=None
        )

    def __init__(
        self, tyngistry: Tyngistry, config: typing.Mapping[str, typing.Any] = None
    ):

        super().__init__(config=config, tyngistry=tyngistry)

    def get_subclass_enum(
        self, cls: typing.Union[typing.Type[BaseModel], typing.Type[Tyng]]
    ):

        if issubclass(cls, Tyng):
            se: SubClassEnum = self._tyngistry.get_tyng_subclass_enum(cls)
        elif issubclass(cls, BaseModel):
            se = self._tyngistry.get_model_subclass_enum(cls)
        else:
            raise TypeError("Invalid type, need 'BaseModel' or 'FrklObject'.")
        return se

    def get_subclasses(
        self, cls: typing.Union[typing.Type[BaseModel], typing.Type[Tyng]]
    ):

        if issubclass(cls, BaseModel):
            return self._tyngistry.get_model_subclasses(cls)
        elif issubclass(cls, Tyng):
            return self._tyngistry.get_tyng_subclasses(cls)
        else:
            raise TypeError("Invalid type, need 'BaseModel' or 'FrklObject'.")

    async def prompt_for_subtype(
        self,
        cls: typing.Union[typing.Type[Tyng], typing.Type[BaseModel]],
        session: PromptSession,
        level: int = 0,
    ) -> typing.Union[typing.Type[Tyng], typing.Type[BaseModel]]:

        type_alias = get_type_alias(cls)

        se = self.get_subclass_enum(cls)
        choices = se.list_type_aliases()

        if not choices:
            raise FrklException(
                f"Can't prompt for choices for type '{type_alias}'.",
                reason="No subclasses available.",
            )

        def choice_validator(text: str):
            return text in choices

        padding = " " * (level + 1)

        msg = f"{padding}Select a '{type_alias}': "

        value = None
        while value is None:
            value = await session.prompt_async(msg, completer=WordCompleter(choices))
            if not choice_validator(value):
                print(f"Invalid choice, select one of: {', '.join(choices)}")
                value = None
            else:
                break

        return se[value].cls

    async def prompt_for_type_details(
        self,
        cls: typing.Union[typing.Type[Tyng], typing.Type[BaseModel]],
        session: PromptSession,
        level: int = 0,
    ) -> typing.Dict[str, typing.Any]:

        if issubclass(cls, Tyng):
            model_cls: typing.Type[BaseModel] = cls.ConfigSchema
        else:
            model_cls = cls

        result: typing.Dict[str, typing.Any] = {}
        type_alias = get_type_alias(cls)

        padding = " " * (level + 1)

        while True:

            for field, field_details in model_cls.__fields__.items():
                assert field != TYPE_MARKER_KEY

                if "parent_type" in field_details.field_info.extra.keys():
                    # means we have a frkl assembly, and we need to ask for the sub-type first
                    type_alias = field_details.field_info.extra["parent_type"]
                    base_cls = self._tyngistry.typistry.get_base_type(type_alias)
                    subclass_type = await self.prompt_for_subtype(
                        base_cls, session=session, level=level
                    )
                    details = await self.prompt_for_type_details(
                        subclass_type, session=session, level=level + 1
                    )
                    details[TYPE_MARKER_KEY] = self._tyngistry.typistry.get_type_alias(
                        subclass_type
                    )
                    result[field] = details
                elif isinstance(field_details.type_, type) and issubclass(
                    field_details.type_, BaseModel
                ):
                    type_alias = self._tyngistry.typistry.get_type_alias(
                        field_details.type_
                    )
                    print(f"{padding}{type_alias}:")
                    details = await self.prompt_for_type_details(
                        field_details.type_, session=session, level=level + 1
                    )
                    result[field] = details
                else:
                    value = await session.prompt_async(f"{padding}{field}: ")
                    result[field] = value

            try:
                model_cls(**copy.deepcopy(result))
                break
            except Exception as e:
                print(e)
                print(f"Invalid inputs. Please enter the details for '{type_alias}':")

        return result

    async def create_config(
        self, cls: typing.Union[typing.Type[BaseModel], typing.Type[Tyng]]
    ):

        type_alias = self._tyngistry.typistry.get_type_alias(cls)

        history_file = self.get_config("history_file")  # type: ignore

        if history_file:
            os.makedirs(os.path.dirname(history_file), exist_ok=True)
            _h: typing.Optional[FileHistory] = FileHistory(history_file)
        else:
            _h = None

        session: PromptSession = PromptSession(history=_h)
        print(f"Please enter the details for '{type_alias}' (enter '?' for help):")

        subclasses = self.get_subclasses(cls)
        if len(subclasses) > 0:
            # means we want to ask which subtype to use
            subclass_type = await self.prompt_for_subtype(cls, session=session)
            result = await self.prompt_for_type_details(subclass_type, session=session)
        else:
            # means we want to get the details for the subtype
            result = await self.prompt_for_type_details(cls, session=session)

        return result

        # results = {}
        #
        # for field, d in model.__fields__.items():
        #
        #     msg = field
        #     # if d.field_info.default:
        #     #     msg = msg + f" ({d.field_info.default})"
        #
        #     value = await session.prompt_async(f"{msg}: ")
        #     results[field] = value
