# -*- coding: utf-8 -*-
import abc
import typing
from pydantic import BaseModel

from frkl.typistry import Tyng
from frkl.typistry.tyngistry import Tyngistry


class DataSource(Tyng, abc.ABC):
    def __init__(
        self,
        tyngistry: Tyngistry,
        config: typing.Optional[typing.Mapping[str, typing.Any]] = None,
    ):

        self._tyngistry: Tyngistry = tyngistry
        Tyng.__init__(self, config=config)

    async def retrieve_config_data(
        self,
        cls: typing.Union[typing.Union[typing.Type[Tyng], typing.Type[BaseModel]]],
        defaults: typing.Optional[typing.Mapping[str, typing.Any]] = None,
    ) -> typing.Dict[str, typing.Any]:

        if defaults:
            raise NotImplementedError()

        if issubclass(cls, Tyng):
            config_cls: typing.Type[BaseModel] = cls.ConfigSchema
        elif issubclass(cls, BaseModel):
            config_cls = cls
        else:
            raise TypeError(
                f"Can't retrieve config for class '{cls}'. Not a Tyng or BaseModel."
            )

        data = await self.create_config(cls=config_cls)

        return data

    @abc.abstractmethod
    async def create_config(
        self, cls: typing.Union[typing.Type[BaseModel], typing.Type[Tyng]]
    ) -> typing.Dict[str, typing.Any]:
        pass
