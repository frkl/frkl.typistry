# -*- coding: utf-8 -*-
import inspect
import typing

from bidict import bidict
from typing import Dict, Iterable, List, Mapping, Optional, Set, Type, Union

from frkl.common.environment import is_debug
from frkl.common.exceptions import FrklException
from frkl.common.strings import generate_valid_identifier
from frkl.common.types import get_all_subclasses, load_modules
from frkl.typistry.core import ALL_MODULES_MARKER, log, RegisteredType
from frkl.typistry.defaults import DEFAULT_TYPELOADER_ID

if typing.TYPE_CHECKING:
    pass

TypeInput = Union["RegisteredType", Type, str]
"""An union of possible types to be used as input for the TypeLoader class."""


class TypeLoader(object):
    """Class to manage loaded modules and types.

    TODO: more documentation

    Args:
      pre_load_modules: a list of modules to pre-load before trying to access any classes
    """

    _instances: Dict[str, "TypeLoader"] = {}

    @classmethod
    def register_typeloader(cls, typeloader: "TypeLoader"):

        if typeloader.typeloader_id in cls._instances.keys():
            raise FrklException(msg="Can't register typeloader.", reason=f"Typeloader with id '{typeloader.typeloader_id}' already registered.")

        cls._instances[typeloader.typeloader_id] = typeloader

    @classmethod
    def instance(cls, type_loader: Union[None, str, "TypeLoader"]=None) -> "TypeLoader":

        if type_loader is None:
            if DEFAULT_TYPELOADER_ID not in cls._instances.keys():
                tl = TypeLoader(typeloader_id=DEFAULT_TYPELOADER_ID)
                cls.register_typeloader(tl)
            return cls._instances[DEFAULT_TYPELOADER_ID]
        elif isinstance(type_loader, str):
            if type_loader not in cls._instances.keys():
                if type_loader == DEFAULT_TYPELOADER_ID:
                    tl = TypeLoader(typeloader_id=DEFAULT_TYPELOADER_ID)
                    cls.register_typeloader(tl)
                    return cls._instances[DEFAULT_TYPELOADER_ID]
                else:
                    if not cls._instances:
                        raise FrklException(msg=f"Can't retrieve typeloader instance with id '{type_loader}'.",
                                            reason="No typeloaders registered (yet).")
                    else:
                        raise FrklException(msg=f"Can't retrieve typeloader instance with id '{type_loader}'.",
                                            reason="No typeloader with this id registered.",
                                            solution=f"Registered typeloader ids: {' '.join(cls._instances.keys())}.")
            return cls._instances[type_loader]
        elif isinstance(type_loader, TypeLoader):
            return type_loader
        else:
            raise TypeError(f"Invalid type, not a TypeLoader: {type(type_loader)}")

    def __init__(self, typeloader_id: Optional[str], pre_load_modules: Optional[Iterable[str]] = None):

        if typeloader_id is None:
            typeloader_id = generate_valid_identifier(name="type_loader_")

        if pre_load_modules is None:
            _preload_modules: Dict[str, Set[str]] = {ALL_MODULES_MARKER: set()}
        else:
            _preload_modules = {ALL_MODULES_MARKER: set(pre_load_modules)}

        self._typeloader_id: str = typeloader_id

        self._module_namespaces_to_preload: Dict[str, Set[str]] = _preload_modules
        """Modules that are scheduled to be loaded (or re-loaded). Key is the bundle alias, value a set of module namespaces."""

        self._preloaded_module_namespaces: Dict[str, Set[str]] = {}
        """Holds all modules the loader already knows about. Key is the bundle alias, value a set of module namespaces."""

        self._type_aliases: bidict[str, RegisteredType] = bidict()
        """A bidirectional map of aliases and registered types."""
        self._subclass_cache: Dict[RegisteredType, Dict[str, RegisteredType]] = {}
        """Cache to store all subclasses of a registered type."""
        self._subclass_cache_bundle_map: Dict[RegisteredType, str] = {}
        """Hint that stores which bundle was loaded when resolving subclasses for a certain type."""

    @property
    def typeloader_id(self) -> str:
        return self._typeloader_id

    def register_module_namespaces(
        self, *module_namespaces: str, bundle_alias: Optional[str] = None
    ) -> List[RegisteredType]:
        """Add module namespaces to load.

        Optionally, a bundle alias can be provided. This can be used to more fine-grainedly load modules if necessary.
        All added modules will be added to the 'meta'-bundle alias '__all_modules__'.

        Arguments:
            *module_namespaces: a list of module namespaces (basically Python module names)
            bundle_alias: an (optional) alias to which to add those namespaces

        Returns:
            a list of all base types that had subclasses registered and where new subclasses could be available after the loading of those new modules
        """

        result: Set[RegisteredType] = set()
        if bundle_alias is None:
            bundle_alias = ALL_MODULES_MARKER
            self._module_namespaces_to_preload.setdefault(bundle_alias, set()).update(
                module_namespaces
            )
            result.update(self._subclass_cache.keys())

            self._subclass_cache.clear()
        else:
            self._module_namespaces_to_preload.setdefault(bundle_alias, set()).update(
                module_namespaces
            )
            self._module_namespaces_to_preload.setdefault(bundle_alias, set()).update(
                ALL_MODULES_MARKER
            )
            if bundle_alias in self._subclass_cache_bundle_map.values():
                for type_alias, bundle_alias in self._subclass_cache_bundle_map.items():
                    if bundle_alias == bundle_alias:
                        result.add(type_alias)
                        self._subclass_cache.pop(type_alias, None)

        return list(result)

    def _load_modules(self, bundle_alias: str = ALL_MODULES_MARKER):
        """Preload modules, conditional on whether a set of modules for a type alias was already loaded or not."""

        preload = False

        # check whether the bundle has the same length, because if it has, we don't need to do any loading
        if len(self._module_namespaces_to_preload[bundle_alias]) != len(
            self._preloaded_module_namespaces.get(bundle_alias, [])
        ):
            preload = True

        if not preload:
            # preloading not necessary, since we've already done it for the currently existing preload modules
            return

        all_modules_to_load = set()
        all_modules_to_load.update(self._module_namespaces_to_preload[bundle_alias])

        for already_loaded in self._preloaded_module_namespaces.setdefault(bundle_alias, set()):
            all_modules_to_load.remove(already_loaded)

        load_modules(*all_modules_to_load)

        self._preloaded_module_namespaces[bundle_alias].update(
            self._module_namespaces_to_preload[bundle_alias]
        )

        if bundle_alias == ALL_MODULES_MARKER:
            for key in self._preloaded_module_namespaces.keys():
                self._preloaded_module_namespaces[key].update(
                    self._module_namespaces_to_preload[key]
                )

    def register_type(
        self,
        cls: TypeInput,
        bundle_alias: str = ALL_MODULES_MARKER,
        allow_existing: bool = False,
    ) -> RegisteredType:
        """Register a Python class into this type loader.
        """

        _validated_cls: Optional[RegisteredType] = None
        try:
            if isinstance(cls, (type, str)):
                _validated_cls = RegisteredType.create_type(cls)
            elif isinstance(cls, RegisteredType):
                # make sure the class actually exists
                cls.cls  # noqa
                _validated_cls = cls
        except Exception:
            # TODO: catch the proper exceptions
            # Python class is not found in currently loaded modules, need to try to load the provided modules in the
            # specified bundle
            self._load_modules(bundle_alias=bundle_alias)
            if isinstance(cls, (type, str)):
                _validated_cls = RegisteredType.create_type(cls)
            elif isinstance(cls, RegisteredType):
                cls.cls  # noqa
                _validated_cls = cls

        if _validated_cls is None:
            raise FrklException(
                msg=f"Can't register type '{cls}'.",
                reason="Can't find the class in the python path.",
                solution="Make sure you either import the module that contains this class before calling this method, or register the module namespace in the type loader.",
            )

        if _validated_cls.alias in self._type_aliases:
            if not allow_existing:
                raise FrklException(
                    msg=f"Can't register type '{_validated_cls.alias}'.",
                    reason="Type already registered.",
                )
        else:
            self._type_aliases[_validated_cls.alias] = _validated_cls
        return self._type_aliases[_validated_cls.alias]

    def get_type(self, cls: TypeInput) -> RegisteredType:
        """Get the registered type model instance for the provided type."""

        if isinstance(cls, (type, str)):
            cls = RegisteredType.create_type(cls)
        elif not isinstance(cls, RegisteredType):
            raise TypeError(f"Invalid type: {type(cls)}")

        if cls.alias not in self._type_aliases.keys():
            self._type_aliases[cls.alias] = cls
        return self._type_aliases[cls.alias]

    def register_subclasses(
        self, cls: TypeInput, bundle_alias: str = ALL_MODULES_MARKER
    ) -> Mapping[str, RegisteredType]:
        """Find and register into this loader all subclasses of the provided type.

        Arguments:
            cls: the type
            bundle_alias: (optional) hint as to which Python modules to load before attempting to find subclasses

        Returns:
            a map of all found subclasses (key/value: alias/registered type)
        """

        r_type = self.register_type(cls, bundle_alias=bundle_alias, allow_existing=True)

        if r_type in self._subclass_cache.keys():
            raise FrklException(
                msg=f"Can't register subclasses for '{r_type.alias}'.",
                reason="Subclasses already registered.",
            )

        self._load_modules(bundle_alias=bundle_alias)
        sc = self.find_subclasses(cls, include_abstract_classes=False)

        for sc_r_type in sc.values():
            self.register_type(sc_r_type, allow_existing=True)

        if bundle_alias != ALL_MODULES_MARKER:
            self._subclass_cache_bundle_map[r_type] = bundle_alias
        self._subclass_cache[r_type] = sc
        return self._subclass_cache[r_type]

    def get_subclasses(self, cls: TypeInput) -> Mapping[str, RegisteredType]:
        """Get all subclasses of the specified type.

        Returns either a cached result, or calls 'register_subclasses' to find and register them if no cache.
        """

        r_type = self.get_type(cls=cls)
        if r_type in self._subclass_cache.keys():
            return self._subclass_cache[r_type]
        else:
            bundle_alias = ALL_MODULES_MARKER
            # TODO: check if this is right, or at least document
            if r_type in self._subclass_cache_bundle_map.keys():
                bundle_alias = self._subclass_cache_bundle_map[r_type]

            return self.register_subclasses(r_type, bundle_alias=bundle_alias)

    def find_subclasses(
        self, base_class: TypeInput, include_abstract_classes: bool = False
    ) -> Dict[str, RegisteredType]:
        """Return all (non-abstract) subclasses of a base class.

        This method does not do any preloading of module bundles, so make sure all the modules you are interested
        in are loaded at this point.

        Arguments:
            base_class: the base class
            include_abstract_classes: whether to include abstract classes in the result
        Returns:
            all subclasses of the base class
        """

        r_type = self.get_type(base_class)

        all_subclasses = get_all_subclasses(r_type.cls)

        subclasses: Dict[str, RegisteredType] = {}
        for sc in all_subclasses:
            if inspect.isabstract(sc) and not include_abstract_classes:
                if is_debug():
                    log.warning(f"Ignoring subclass '{sc}': is abstract")
                else:
                    log.debug(f"Ignoring subclass '{sc}': is abstract")
                continue

            log.debug(f"Adding subclass: {sc}")

            sc_r_type = RegisteredType.create_type(sc)
            subclasses[sc_r_type.alias] = sc_r_type

        return subclasses

    def find_abstract_subclasses(
        self, base_class: TypeInput
    ) -> Dict[str, RegisteredType]:

        result = self.find_subclasses(
            base_class=base_class, include_abstract_classes=True
        )
        return {k: v for k, v in result.items() if inspect.isabstract(v.cls)}
