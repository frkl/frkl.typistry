import os

import logging

log = logging.getLogger("frkl.typistry")

def is_debug() -> bool:

    debug = os.environ.get("DEBUG", None)
    if debug.lower() == "true":
        return True
    else:
        return False

def is_develop() -> bool:

    debug = os.environ.get("DEVELOP", None)
    if debug.lower() == "true":
        return True
    else:
        return False

def skip_instance_tests() -> bool:

    skip = os.environ.get("SKIP_INSTANCE_TESTS", None)
    if skip.lower() == "true":
        return True
    else:
        return False

def log_message(msg: str):

    if is_debug():
        log.warning(msg)
    else:
        log.debug(msg)
