# -*- coding: utf-8 -*-
import importlib
import logging
from enum import Enum
from functools import partial, wraps
from types import ModuleType
from typing import Tuple, Type, Union, Optional, Mapping, Iterable, MutableMapping, TypeVar, TYPE_CHECKING, Generic

from frkl.common.exceptions import FrklException

from frkl.common.strings import from_camel_case

if TYPE_CHECKING:
    from pydantic.decorator import ValidatedFunction
    from pydantic import BaseModel

log = logging.getLogger("frkl.common")


def extract_single_literal_key(cls: Type) -> Optional[str]:

    if not hasattr(cls, "__fields__"):
        return None

    if TYPE_MARKER_KEY not in cls.__fields__.keys():
        return None

    _type = cls.__fields__[TYPE_MARKER_KEY].type_
    if not hasattr(_type, "__args__"):
        return None

    args = _type.__args__
    if len(args) != 1:
        return None

    arg = args[0]
    if not isinstance(arg, str):
        return None

    return arg


def get_type_alias(cls: Type) -> str:

    assert isinstance(cls, type)

    type_alias = extract_single_literal_key(cls)
    if type_alias is not None:
        return type_alias

    if hasattr(cls, TYPE_ALIAS_ATTRIBUTE):
        return cls._type_alias  # type: ignore
    else:
        return from_camel_case(cls.__name__)


ALL_MODULES_MARKER = "__all_mdules__"
TYPE_MARKER_KEY = "type"
TYPE_ALIAS_ATTRIBUTE = "_type_alias"


class SubClassEnum(Enum):
    @classmethod
    def list_type_aliases(cls):
        return list(map(lambda c: c.value, cls))

    @classmethod
    def list_types(cls):
        return list(map(lambda c: c.cls, cls))

    @classmethod
    def type_hint(cls, optional: bool = False):

        if not optional:
            return Union[tuple(cls.list_types())]
        else:
            return Optional[Union[tuple(cls.list_types())]]

    def __new__(cls, *args, **kwds):
        type_name = get_type_alias(args[0])
        obj = object.__new__(cls)
        obj._value_ = type_name
        return obj

    def __init__(self, cls: Type):
        setattr(self, "prototing_class", cls)


class ModelSubClassEnum(SubClassEnum):
    def __new__(cls, *args, **kwds):
        type_name = get_type_alias(args[0])
        obj = object.__new__(cls)
        obj._value_ = type_name
        return obj

    def __init__(self, cls: Type):
        setattr(self, "prototing_class", cls)
        # setattr(self, "schema_json", prototing_class.schema_json)

REGISTERED_TYPE_CLS = TypeVar("REGISTERED_TYPE_CLS")
class RegisteredType(Generic[REGISTERED_TYPE_CLS]):
    """A model for a registered type.

    Contains constructor information and some other metadata and helper methods.
    """
    @classmethod
    def split_full_class_name(cls, class_name: str) -> Tuple[str, str]:

        if "." in class_name:
            module_name, cls_name = class_name.rsplit(".", maxsplit=1)
        else:
            module_name = ""
            cls_name = class_name

        return (module_name, cls_name)

    @classmethod
    def get_type(cls, alias: str) -> Type:
        if "." in alias:
            module_name, cls_name = alias.rsplit(".", 1)
        else:
            module_name = ""
            cls_name = alias

        _module_obj = importlib.import_module(module_name)
        result = getattr(_module_obj, cls_name)

        return result

    @classmethod
    def get_alias(cls, cls_obj: Type):

        cls_name = cls_obj.__name__
        module_name = cls_obj.__module__

        if not module_name:
            return cls_name
        else:
            return f"{module_name}.{cls_name}"

    @classmethod
    def create_type(self, cls: Union[str, Type]):
        if isinstance(cls, str):
            # ensure class is available
            _ = RegisteredType.get_type(cls)
            module_name, cls_name = RegisteredType.split_full_class_name(cls)
        elif isinstance(cls, type):
            alias = RegisteredType.get_alias(cls)
            module_name, cls_name = RegisteredType.split_full_class_name(alias)
        else:
            raise TypeError(f"Invalid type, must be string or Python class: {cls}")

        return RegisteredType(module_name=module_name, type_name=cls_name)

    def __init__(self, module_name: str, type_name: str):

        self._cls: Optional[Type[REGISTERED_TYPE_CLS]] = None
        self._module: ModuleType = None
        self._constructor_validator: Optional[ValidatedFunction] = None

        self._module_name: str = module_name
        self._type_name: str = type_name

    @property
    def module_name(self) -> str:
        return self._module_name

    @property
    def type_name(self) -> str:
        return self._type_name

    @property
    def module(self) -> ModuleType:
        if self._module is None:
            self._module = importlib.import_module(self.module_name)
        return self._module

    @property
    def cls(self) -> Type[REGISTERED_TYPE_CLS]:
        if self._cls is None:
            self._cls = getattr(self.module, self.type_name)
        return self._cls

    @property
    def construction_validator(self) -> "ValidatedFunction":
        """A validator for the constructor of this class."""

        if self._constructor_validator is None:
            try:
                from pydantic.decorator import ValidatedFunction
            except Exception:
                raise FrklException(msg=f"Can't extract constructor validator for '{self.type_name}': 'pydantic' library not available")
            self._constructor_validator = ValidatedFunction(function=self.cls.__init__, config=None)
        return self._constructor_validator

    @property
    def constructor_model(self) -> "BaseModel":
        return self.construction_validator.model

    @property
    def alias(self) -> str:
        return f"{self.module_name}.{self.type_name}"

    # def create_obj(self, *args, **kwargs):
    #     """Create an instance of this registered Python class."""
    #
    #     # return self.construction_validator.call(*args, **kwargs)
    #     # TODO: Validate constructor
    #     return self.prototing_class(*args, **kwargs)

    def __eq__(self, other):

        if not isinstance(other, RegisteredType):
            return False

        return self.cls == other.cls

    def __hash__(self):

        return hash(self.cls)

    def __repr__(self):

        return (
            f"RegisteredType(module_name={self.module_name} type_name={self.type_name})"
        )

ConfigValueType = Union[str, int, float, bool, "ConfigMapping", Iterable["ConfigMapping"]]
ConfigMapping = Mapping[str, ConfigValueType]
MutableConfig = MutableMapping[str, ConfigValueType]
