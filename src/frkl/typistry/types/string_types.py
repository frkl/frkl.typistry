# -*- coding: utf-8 -*-
from pydantic import StrictStr


class ValidIdentifier(StrictStr):
    """Valid Python identifier validation."""

    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not isinstance(v, str):
            raise TypeError("string required")

        if not v.isidentifier():
            raise ValueError("Invalid format, string is not an identifier.")

        return v

    def __repr__(self):
        return f"PostCode({super().__repr__()})"


class ValidModuleName(StrictStr):
    """Valid Python identifier validation."""

    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not isinstance(v, str):
            raise TypeError("string required")

        for token in v.split("."):
            if not token.isidentifier():
                raise ValueError("Invalid format, string is not an identifier.")

        return v

    def __repr__(self):
        return f"PostCode({super().__repr__()})"
