from abc import ABC, abstractmethod
from typing import Optional, Any, Union, TYPE_CHECKING

from frkl.common.doc import Doc
from frkl.common.exceptions import FrklException
from frkl.tings.defaults import TING_TYPE_ALIAS, DEFAULT_PROTOTING_ID
from frkl.tings.utils import check_id_valid
from frkl.typistry.core import ConfigMapping, ConfigValueType

if TYPE_CHECKING:
    from frkl.tings.tingistry import Tingistry
    from frkl.tings.payload import TingFabPayload, TingInstancePayload, ConfigPayload
    from frkl.tings.meta import TingMeta


TingInput = Union[str, "Ting"]

class Ting(ABC):

    def __init__(self, ting_meta: "TingMeta", ting_config: Optional["ConfigPayload"]=None):

        if not ting_meta:
            raise FrklException(msg=f"Can't create ting instance.", reason="Provided metadata is empty.")
        self._ting_meta: "TingMeta" = ting_meta

        self._ting_config: Optional[ConfigPayload] = ting_config

        self._ting_payload: Any = None

    @property
    def _ting_id(self) -> str:
        return self._ting_meta.ting_id

    @property
    def _ting_doc(self) -> Doc:
        return self._ting_meta.doc

    def _ting_get_payload(self) -> Any:
        return self._ting_payload

    @abstractmethod
    def _ting_get_payload_type(self) -> str:
        pass

    def _ting_set_payload(self, ting_payload: Any):

        payload_type = self._ting_get_payload_type()
        tingistry = self._ting_retrieve_tingistry()
        ting_payload_type = tingistry.get_payload_type(payload_ting_id=payload_type)

        ting_payload_type.parse_payload(ting_payload)

    def _ting_retrieve_tingistry(self) -> "Tingistry":
        from frkl.tings.tingistry import Tingistry
        return Tingistry.instance(self._ting_meta.tingistry_id)

    def _ting_send_message(self, msg: str):
        pass

    def __eq__(self, other):
        if other.__class__ != self.__class__:
            return False
        # TODO: is this enough?
        return self._ting_id == other._ting_id

    def __hash__(self):
        return hash(self._ting_id)

class SimpleTing(Ting):

    def _ting_get_payload_type(self) -> str:
        return "any"

    # def _ting_set_payload(self, ting_payload: Any):
    #
    #     if hasattr(self, "_ting_validate_payload"):
    #         ting_payload = self._ting_validate_payload(ting_payload)
    #     self._ting_payload = ting_payload

class ConfigTing(Ting):

    def _ting_get_payload_type(self) -> str:
        return "payload.config"

    def _ting_patch_payload(self, key: str, value: ConfigValueType):
        pass


class DefaultProtoTing(Ting):

    _type_alias = "default_prototing"

    def _ting_get_payload_type(self) -> str:
        return "none"

    def _ting_set_payload(self, ting_payload: Any):
        assert ting_payload is None

    def _ting_create(self, ting_meta: "TingMeta", ting_config: Optional["ConfigPayload"]=None) -> "Ting":

        tingistry = self._ting_retrieve_tingistry()
        ting_r_type = tingistry.typistry.get_subtype(TING_TYPE_ALIAS, ting_meta.ting_type)
        config = None
        ting = ting_r_type.cls(ting_meta=ting_meta, ting_config=config)
        return ting


class PydanticModelTing(Ting):

    _type_alias = "pydantic_model"

    def _ting_get_payload_type(self) -> str:
        return "config_map"

    def _ting_set_payload(self, ting_payload: Any):
        pass

class TingFab(Ting):

    def __init__(self, ting_id: str, ting_payload: "TingFabPayload", ting_meta: Optional["TingMeta"]=None):

        super().__init__(ting_id=ting_id, ting_payload=ting_payload, ting_meta=ting_meta)




