from typing import Mapping, Optional, Union

from pydantic import BaseModel, Field

from frkl.common.doc import Doc
from frkl.typistry.core import ConfigMapping

TingMetaInput = Union[str, None, Doc, ConfigMapping]

class TingMeta(BaseModel):

    @classmethod
    def create(cls, ting_id: str, ting_type: str, tingistry_id: str, metadata: TingMetaInput=None) -> "TingMeta":

        if not metadata:
            doc = Doc()
        elif isinstance(metadata, str):
            doc = Doc(metadata)
        elif isinstance(metadata, Doc):
            doc = metadata
        elif isinstance(metadata, Mapping):
            doc = metadata.get("doc", None)
        else:
            raise TypeError(
                f"Can't create TingMeta object, invalid input type: {type(metadata)}"
            )


        return TingMeta(ting_id=ting_id, tingistry_id=tingistry_id, ting_type=ting_type, doc=doc)

    ting_id: str = Field(description="The id of this ting.")
    ting_type: str = Field(description="The alias of the type of this ting within it's parent tingistry.")
    payload_type: Optional[str] = Field(description="The ting id that describes the ting_payload.")
    tingistry_id: str = Field(description="A reference to the tingistry that created holds this ting.")
    doc: Doc = Field(description="The documentation for this prototing.", default=Doc)


