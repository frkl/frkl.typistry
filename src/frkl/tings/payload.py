from abc import abstractmethod
from typing import Any, Mapping

from pydantic import BaseModel, Field

from frkl.tings.core import Ting


class PayloadType(Ting):

    def _ting_get_payload_type(self) -> str:
        return "config_mapping"

    def parse_payload(self, ting_payload: Any) -> Any:
        raise NotImplementedError()

    def serialize_payload(self) -> Any:
        raise NotImplementedError()

    def deserialize_payload(self) -> Any:
        raise NotImplementedError()

class NonePayloadType(PayloadType):

    _type_alias = "payload.none"

    def parse_payload(self, ting_payload: Any) -> Any:

        assert ting_payload is None
        return None

class AnyPayloadType(PayloadType):

    _type_alias = "payload.any"

    def parse_payload(self, ting_payload: Any) -> Any:
        return ting_payload


class ConfigPayloadType(PayloadType):

    _type_alias = "payload.config"

    def parse_payload(self, ting_payload: Any) -> Any:

        if not isinstance(ting_payload, Mapping):
            raise TypeError(f"Invalid type '{type(ting_payload)}' for config payload, needs a mapping.")

        # TODO: check mapping only contains valid types
        return ting_payload


class PythonObjectPayloadType(PayloadType):

    _type_alias = "payload.python_obj"

    pass


class ModelPayload(BaseModel):
    pass

class ConfigPayload(ModelPayload):
    pass

class PythonObjectPayload(ModelPayload):

    _type_alias = "python_object"

    ting_type: str = Field(description="The type of ting this prototing produces.")
    # ting_cls: str = Field(description="The class of the Ting instance to create.")
    # prototing_config: Dict[str, Any] = Field(default_factory=dict, description="Configuration for this prototing")
    # ting_default_payload: Any = Field(default=None, description="Default ting_payload, to be used when no initial ting_payload is specified.")

class TingInstancePayload(ModelPayload):

    _type_alias = "ting_instance"

    ting_type: str = Field(description="The type of ting to create.")


class TingFabPayload(ModelPayload):

    pass
