from typing import runtime_checkable, Protocol, Any, Optional

from frkl.tings.core import Ting
from frkl.tings.meta import TingMeta
from frkl.tings.payload import ConfigPayload


class TingQuality(Ting):

    def _ting_get_payload_type(self) -> str:
        return "config_mapping"

    def _ting_set_payload(self, ting_payload: Any):
        pass


class ProtoTing(TingQuality):

    _type_alias = "quality.prototing"

    def _ting_create(self, meta: TingMeta, ting_config: Optional[ConfigPayload]=None):
        pass
