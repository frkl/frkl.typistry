import typing

from frkl.tings.core import SimpleTing
from pydantic import BaseModel

# class ProjectDetails(ConfigTing):
#
#     class ProjectDetailsModel(BaseModel):
#         full_name: str = Field(description="The author name.")
#         email: EmailStr = Field(description="The author email.")
#         project_name: str = Field(description="The project name (can't contain whitespace).")  # type: ignore
#         project_slug: str = Field(
#             description="The project slug.",
#             default_desc="Slugified 'project_name' value (can't contain whitespace nor punctuation).",
#         )
#         project_title: str = Field(
#             description="The project title.", default_desc="The project title."
#         )
#         project_short_description: str = Field(
#             description="The project description.", default="-- n/a --"
#         )
#         project_url: Optional[str] = Field(
#             description="The project homepage.",
#             default=None,
#             default_desc="The project repo url.",
#         )
#
#         @validator("project_name", pre=True)
#         def _validate_project_name_exists(cls: Type['Model'], value: Any) -> 'Model':
#
#             if not value:
#                 raise ValueError("No project name provided.")
#
#             if not isinstance(value, str):
#                 raise ValueError(f"Project name must be a string, not: {type(value)}.")
#
#             if " " in value:
#                 raise ValueError(f"Project name can't contain whitespaces: '{value}'")
#
#             return value
#
#         @root_validator(pre=True)
#         def _fill_defaults(cls, values):
#
#             project_name = values.get("project_name", None)
#             if not project_name:
#                 return values
#             if not values.get("project_slug", None):
#                 values["project_slug"] = slugify(values["project_name"], separator="_")
#             if not values.get("project_title", None):
#                 values["project_title"] = values["project_name"]
#
#             return values
#
#         @validator("project_slug")
#         def _validate_default_project_slug(cls, v, *, values, **kwargs):
#
#             assert "." not in v
#             assert "-" not in v
#             assert "\t" not in v
#             assert "\n" not in v
#             assert " " not in v
#
#             # TODO: make this a regex, this needs a lot more characters to test for
#
#             return v
#
#         @validator("project_name", pre=True)
#         def _validate_project_name(cls, v):
#
#             assert isinstance(v, str)
#             assert " " not in v
#             assert "\t" not in v
#             assert "\n" not in v
#
#             return v


class BaseProjectType(SimpleTing):
    pass

class PythonProjectType(BaseProjectType):

    _type_alias = "python_project"
    class ConfigSchema(BaseModel):
        full_name: str
        test: str = "HEY"

class JavaProjectType(BaseProjectType):
    _type_alias = "java_project"

