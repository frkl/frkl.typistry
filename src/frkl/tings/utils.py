import pp
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from frkl.tings.tingistry import Tingistry
    from frkl.typistry import Typistry

def print_typistry_info(typistry: "Typistry"):

    print("Registered types")
    pp(sorted(typistry.registered_types))
    print()
    print("Registered subtypes")
    pp(typistry.registered_subtypes)


def print_tingistry_info(tingistry: "Tingistry", also_print_typistry_info: bool=True):

    if also_print_typistry_info:
        print_typistry_info(tingistry.typistry)
        print()

    print("Registered tings")
    pp(sorted(tingistry.registered_tings))


def check_id_valid(id: str):

    if not id:
        valid = False
    else:
        if not isinstance(id, str):
            valid = False
        else:
            valid = all((x.isidentifier() for x in id.split(".")))

    if not valid:
        raise ValueError(f"Invalid id: {id}")
