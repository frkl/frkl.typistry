# -*- coding: utf-8 -*-
import time
from typing import (
    Any,
    Dict,
    Optional,
    Union, List, Mapping, Iterable, )

from pydantic import BaseSettings, BaseModel, Field, validator

from frkl.common.exceptions import FrklException
from frkl.common.strings import find_free_name, from_camel_case, generate_valid_identifier
from frkl.tings.core import Ting, DefaultProtoTing
from frkl.tings.quality import TingQuality, ProtoTing
from frkl.tings.payload import PayloadType, TingInstancePayload, ConfigPayload
from frkl.tings.utils import check_id_valid
from frkl.tings.defaults import TING_TYPE_ALIAS, TINGS_BASE_NAMESPACE, TING_QUALITY_TYPE_ALIAS, \
    TING_PAYLOAD_TYPE_ALIAS, DEFAULT_PROTOTING_ID, DEFAULT_TINGISTRY_ID, TINGS_NAMESPACE
from frkl.tings.meta import TingMeta, TingMetaInput
from frkl.typistry import Typistry
from frkl.typistry.core import RegisteredType
from frkl.typistry.defaults import DEFAULT_TYPISTRY_ID
from frkl.typistry.utils import skip_instance_tests


def default_type_alias_generator(r_type: RegisteredType):
    return ".".join((from_camel_case(token) for token in r_type.alias.split(".")))


def default_subclass_alias_generator(r_type: RegisteredType):

    return from_camel_case(r_type.type_name)


class ProtoTingDesc(BaseModel):

    prototing_id: str = Field(description="The prototing id.", default_factory=generate_valid_identifier)
    # prototing_type: str = Field(description="The ProtoTing type to use.", default=DEFAULT_PROTOTING_TYPE_ID)
    prototing_config: Dict[str, Any] = Field(default_factory=dict, description="The configuration for this ProtoTing.")


class TingistryConfig(BaseSettings):

    @classmethod
    def create(cls, config: Union["TingistryConfig", Mapping[str, Any], None]):

        if config is None:
            config = TingistryConfig()
        elif isinstance(config, Mapping):
            config = TingistryConfig(**config)
        elif not isinstance(config, TingistryConfig):
            raise TypeError(f"Can't create TingistryConfig object: invalid type '{type(config)}'.")

        return config

    id: str = Field(description="The id for this tingistry.", default_factory=generate_valid_identifier)
    mgmt_port: int = Field(default=7000, description="The management port for this Tingistry.")
    event_port: int = Field(default=7003, description="The port to listen to ting events for this Tingistry.")
    module_namespaces: List[str] = Field(default_factory=list, description="A list of Pythno modules to load.")
    prototings: List[ProtoTingDesc] = Field(default_factory=list, description="A list of prototings to (pre-)register.")
    typistry_id: str = Field(default=DEFAULT_TYPISTRY_ID, description="The id of the typistry to use.")

    @validator('id')
    def id_validator(cls, v):
        check_id_valid(v)


class Tingistry(object):
    """Management class for Tings.

    Preregistered Tings:
    - __tings__.
    """

    _instances: Dict[str, "Tingistry"] = {}

    @classmethod
    def register_tingistry(cls, tingistry: "Tingistry"):

        if tingistry.tingistry_id in cls._instances.keys():
            raise FrklException(msg="Can't register tingistry.", reason=f"Tingistry with id '{tingistry.tingistry_id}' already registered.")

        cls._instances[tingistry.tingistry_id] = tingistry

    @classmethod
    def instance(cls, tingistry: "TingistryRef") -> "Tingistry":

        if tingistry is None:
            _instance = Tingistry(
                id="default_tingistry"
            )
            cls.register_tingistry(_instance)
            return _instance
        elif isinstance(tingistry, str):
            if tingistry not in cls._instances.keys():
                if not cls._instances:
                    if tingistry == DEFAULT_TINGISTRY_ID:
                        _instance = Tingistry(
                            id="default_tingistry"
                        )
                        cls.register_instance(_instance)
                        return cls._instances[tingistry]
                    else:
                        raise FrklException(msg=f"Can't retrieve tingistry instance with id '{tingistry}'.",
                                            reason="No tingistries registered (yet).")
                else:
                    raise FrklException(msg=f"Can't retrieve tingistry instance with id '{tingistry}'.",
                                        reason="No tingistry with this id registered.",
                                        solution=f"Registered typistry ids: {' '.join(cls._instances.keys())}.")
            return cls._instances[tingistry]
        elif isinstance(tingistry, Mapping):
            config = TingistryConfig(**tingistry)
            _instance = Tingistry(tingistry_config=config)
            cls.register_tingistry(_instance)
            return _instance
        elif isinstance(tingistry, Tingistry):
            if tingistry.tingistry_id not in cls._instances:  # maybe check for actual equality instead?
                cls.register_tingistry(tingistry)
            return tingistry
        elif isinstance(tingistry, TingistryConfig):
            _instance = Tingistry(tingistry_config=tingistry)
            cls.register_tingistry(_instance)
            return _instance
        else:
            raise TypeError(f"Invalid type, not a Tingistry: {type(tingistry)}")

    # @classmethod
    # def create_and_manage(cls, typistry: Optional[Typistry]=None, tingistry_config: Optional[TingistryConfig]=None):
    #
    #
    #     def t():
    #         tingistry = cls.create(typistry=typistry, tingistry_config=tingistry_config)
    #         tingistry.start_mgmt()
    #
    #     thread = threading.Thread(group=None, target=t)
    #     return thread


    def __init__(self, tingistry_id: Optional[str]=None, tingistry_config: Optional[TingistryConfig]=None):

        if tingistry_id is None:
            tingistry_id = generate_valid_identifier(prefix="tingistry_", length_without_prefix=8)

        if tingistry_config is None:
            tingistry_config = TingistryConfig()

        self._invalidated: bool = True
        self._tingistry_id = tingistry_id

        self._config: TingistryConfig = tingistry_config
        self._typistry: Typistry = Typistry.instance(self._config.typistry_id)

        self._tings: Dict[str, Ting] = {}

        # self._zmq_ctx: zmq.Context = zmq.Context()
        # self._mgmt_socket = self._zmq_ctx.socket(zmq.REP)
        # self._mgmt_socket.bind(f"tcp://*:{self._config.mgmt_port}")
        # self._event_socket = self._zmq_ctx.socket(zmq.SUB)
        # self._event_socket.bind(f"tcp://*:{self._config.event_port}")

        # misc initialization tasks
        if self._config.module_namespaces:
            self._typistry.register_module_namespaces(*self._config.module_namespaces)

        # register internally used Python classes
        self._typistry.register_type(cls=Ting, type_alias=TING_TYPE_ALIAS)
        self._typistry.register_type(cls=TingQuality, type_alias=TING_QUALITY_TYPE_ALIAS)
        self._typistry.register_type(cls=PayloadType, type_alias=TING_PAYLOAD_TYPE_ALIAS)

        # register some important sub-classes
        self._typistry.register_subtypes(base_type=TING_TYPE_ALIAS)
        self._typistry.register_subtypes(base_type=TING_PAYLOAD_TYPE_ALIAS)
        self._typistry.register_subtypes(base_type=TING_QUALITY_TYPE_ALIAS)

        # create the default prototing, so all other prototings can be created from this
        meta = TingMeta.create(ting_id=DEFAULT_PROTOTING_ID, ting_type=DefaultProtoTing._type_alias, tingistry_id=self.tingistry_id)
        self._tings[DEFAULT_PROTOTING_ID] = DefaultProtoTing(ting_meta=meta)

    def _tingistry_init(self):

        if self._invalidated:
            for payload_type, r_type in self._typistry.get_subtypes(base_type=TING_PAYLOAD_TYPE_ALIAS).items():
                self.create_ting(ting_type=payload_type, ting_id=f"{TINGS_BASE_NAMESPACE}.{payload_type}")

            for quality_type, r_type in self._typistry.get_subtypes(base_type=TING_QUALITY_TYPE_ALIAS).items():
                self.create_ting(ting_type=payload_type, ting_id=f"{TINGS_BASE_NAMESPACE}.{quality_type}")

            self._invalidated = False

    @property
    def tingistry_id(self) -> str:
        return self._tingistry_id

    @property
    def typistry(self) -> Typistry:
        return self._typistry

    @property
    def registered_tings(self) -> Iterable[str]:
        return self._tings.keys()

    def get_quality(self, quality: str) -> RegisteredType[TingQuality]:
        quality_r_type = self.typistry.get_subtype(base_type=TING_QUALITY_TYPE_ALIAS, type_alias=quality)
        return quality_r_type

    def has_all_ting_qualities(self, ting: Ting, *qualities: str) -> False:

        for quality in qualities:
            q = self.get_quality(quality)
            if not isinstance(ting, q.cls):
                return False
        return True

    def start_mgmt(self):

        while True:
            time.sleep(1)
            print("mgmt")

    def register_module_namespaces(
        self, *module_namespaces: str, bundle_alias: Optional[str] = None
    ):

        self._typistry.register_module_namespaces(*module_namespaces, bundle_alias=bundle_alias)

    # def register_type(
    #     self,
    #     prototing_class: TypeInput,
    #     type_alias: Union[None, str, Callable] = None,
    #     allow_existing: bool = False,
    # ) -> str:
    #
    #     return self._typistry.register_type(prototing_class=prototing_class, type_alias=type_alias, allow_existing=allow_existing)

    def create_ting(
        self,
        ting_type: str,
        ting_id: str = None,
        meta: Optional[TingMetaInput] = None,
        ting_config: Optional[ConfigPayload] = None,
        ting_payload: TingInstancePayload = None,
        prototing_id: str = DEFAULT_PROTOTING_ID,
        auto_find_id: bool = False,
    ) -> Ting:

        if ting_id == None:
            ting_id = f"{TINGS_NAMESPACE}.{ting_type}.{generate_valid_identifier(length_without_prefix=12)}"

        check_id_valid(ting_id)

        if ting_id in self._tings.keys():
            if auto_find_id:
                ting_id = self.find_free_ting_name(ting_id)
            else:
                raise FrklException(
                    msg=f"Can't register ting with id '{ting_id}'.",
                    reason="A ting with that id already exists.",
                )

        prototing = self.get_prototing(prototing_ting_id=prototing_id)
        ting_meta = TingMeta.create(ting_id=ting_id, tingistry_id=self.tingistry_id, ting_type=ting_type, metadata=meta)

        ting = prototing._ting_create(ting_meta=ting_meta, ting_config=ting_config)
        # ting_type = self._typistry.get_subtype(TING_TYPE_ALIAS, prototing.ting_type)
        # ting = ting_type.cls(id=ting_id, ting_payload=ting_payload, meta=meta)

        self._tings[ting_id] = ting

        return self._tings[ting_id]

    # def register_prototing(
    #     self,
    #     prototing_ting_id: Optional[str] = None,
    #     prototing_config: Optional[ConfigMapping] = None,
    #     prototing_type: str = DEFAULT_PROTOTING_TYPE_ID,
    #     meta: TingMetaInput = None,
    #     allow_existing: bool = False,
    # ) -> str:
    #     """Register a new prototing.
    #
    #     Arguments:
    #         prototing_ting_id: a (ting-)id of the prototing, used to create tings later on
    #         prototing_config: (optional) configuration for this prototing
    #         prototing_type: the type of the prototing (aka it's class), in most cases you'll want the default
    #         meta: metadata for this prototing
    #         allow_existing: if set to False, an exception will be thrown if this prototing already exists, alternative not implemented yet
    #
    #     Returns:
    #         the prototing ref string
    #     """
    #
    #     if prototing_ting_id is None:
    #         prototing_alias = generate_valid_identifier(name=prototing_type)
    #         prototing_ting_id = f"{TINGS_BASE_NAMESPACE}.prototings.{prototing_alias}"
    #
    #     check_id_valid(prototing_ting_id)
    #
    #     if prototing_ting_id in self._tings.keys():
    #         if allow_existing:
    #             raise NotImplementedError()
    #         else:
    #             raise FrklException(
    #                 msg=f"Can't register prototing '{prototing_ting_id}'.",
    #                 reason="Prototing id already registered.",
    #             )
    #
    #     # TODO: check prototing type exists?
    #     # prototing_r_type = self.typistry.get_subtype(base_type=PROTOTING_TYPE_ALIAS, type_alias=prototing_type)
    #
    #     prototing_payload = {
    #         "ting_type": PROTOPROTOTING_TYPE_ALIAS,
    #         "prototing_config": {
    #             "ting_type": prototing_type,
    #             "prototing_config": prototing_config,
    #         }
    #     }
    #
    #     self.create_ting(prototing_id=PROTOPROTOTING_TYPE_ALIAS, ting_id=prototing_ting_id, ting_payload=prototing_payload, meta=meta, allow_existing=False)
    #     return prototing_ting_id

    def get_prototing(self, prototing_ting_id: Optional[str] = DEFAULT_PROTOTING_ID) -> ProtoTing:

        if prototing_ting_id == DEFAULT_PROTOTING_ID:
            return self._tings[DEFAULT_PROTOTING_ID]  # type: ignore

        if prototing_ting_id not in self._tings.keys():
            raise FrklException(
                    msg=f"Can't retrieve prototing with id '{prototing_ting_id}'.",
                    reason="No prototing with this id registered."
            )

        prototing = self.get_ting(prototing_ting_id)
        if not skip_instance_tests():
            if not self.has_all_ting_qualities(prototing, "quality.prototing"):
                raise FrklException(msg=f"Can't retrieve prototing with id '{prototing_ting_id}'.", reason="Ting with this id exists, but is not of type ProtoTing.")
        return prototing

    def get_payload_type(self, payload_ting_id: str):

        if payload_ting_id not in self._tings.keys():
            raise FrklException(
                msg=f"Can't retrieve payload type with id {payload_ting_id}.",
                reason="No payload type with this id registered."
            )
        payload_ting = self.get_ting(payload_ting_id)
        if not skip_instance_tests():
            if not isinstance(payload_ting, PayloadType):
                raise FrklException(msg=f"Can't retrieve payload type with id {payload_ting_id}.", reason="Ting with this id exists, but is not a 'PayloadTing'.")

        return payload_ting

    def find_free_ting_name(self, base_name: str) -> str:

        existing = set()

        for n in self._tings.keys():
            if not n.startswith(base_name):
                continue

            extra = n[len(base_name) :].split(".")[0]  # noqa
            existing.add(extra)

        if "." in base_name:
            namespace, name = base_name.rsplit(".", 1)
        else:
            namespace = None
            name = base_name

        ting_name = find_free_name(
            name, existing, method="count", method_args={"try_no_postfix": False}
        )
        if namespace:
            return f"{namespace}.{ting_name}"
        else:
            return ting_name

    def get_ting(self, ting_id) -> Ting:

        if ting_id not in self._tings.keys():
            raise FrklException(
                msg=f"Can't retrieve ting with id '{ting_id}'.",
                reason="No ting with that id registered.",
            )

        return self._tings[ting_id]

    def get_obj(self, ting_id: str) -> Any:
        return self.get_ting(ting_id=ting_id).obj

TingistryRef = Union[Tingistry, TingistryConfig, str, None, Mapping]


# class TingistryThread(threading.Thread):
#
#     @classmethod
#     def create_and_start(cls, tingistry_config: Union[TingistryConfig, Mapping, None]=None):
#
#         tt = cls(TingistryConfig.create(tingistry_config))
#         tt.start()
#         return tt
#
#     def __init__(self, tingistry_config: Union[TingistryConfig, Mapping, None]):
#
#         self._tingistry_config: TingistryConfig = TingistryConfig.create(tingistry_config)
#
#         super().__init__(group=None, target=self.start_service)
#
#     def start_service(self):
#
#         tingistry = Tingistry.create(tingistry_config=self._tingistry_config)
#         tingistry.start_mgmt()


