#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `frkl_typistry` package."""
from pathlib import Path

import pytest  # noqa

import frkl.typistry
from frkl.typistry.type_loader import TypeLoader
from frkl.typistry.core import RegisteredType
from frkl.typistry.typistry import Typistry

RESOURCES_FOLDER = Path(__file__).parent / "resources"

def test_library_version():

    assert frkl.typistry.get_version() is not None


def test_type_loader():

    tl = TypeLoader(pre_load_modules=["example_types"])
    rt = tl.get_type("example_types.example_1.ExampleType1")
    assert isinstance(rt, RegisteredType)

    sc = tl.get_subclasses(rt)
    assert len(sc) == 2
    assert "example_types.example_1.Child1ExampleType1" in sc.keys()
    assert "example_types.example_1.Child2ExampleType1" in sc.keys()


def test_typistry():

    tl = TypeLoader(pre_load_modules=["example_types"])
    ty = Typistry(type_loader=tl)

    assert not ty.registered_type_aliases

    type_name = ty.register_type("example_types.example_1.ExampleType1")
    assert type_name == "example_types.example_1.example_type1"

    sc = ty.get_subtypes(type_name)
    assert len(sc) == 2

    assert "child1_example_type1" in sc.keys()
    assert "child2_example_type1" in sc.keys()

def test_typistry_1():

    tl = TypeLoader(pre_load_modules=["example_types"])
    ty = Typistry(type_loader=tl)

    assert not ty.registered_type_aliases

    type_name = ty.register_type("example_types.example_1.ExampleType1", type_alias="ExTy1")
    assert type_name == "ExTy1"

    sc = ty.get_subtypes(type_name)
    assert len(sc) == 2

    assert "child1_example_type1" in sc.keys()
    assert "child2_example_type1" in sc.keys()
