#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `frkl_typistry` package."""
from pathlib import Path

import pytest  # noqa

import frkl.typistry
from frkl.tings.tingistry import Tingistry
from frkl.typistry.type_loader import TypeLoader
from frkl.typistry.core import RegisteredType
from frkl.typistry.typistry import Typistry

RESOURCES_FOLDER = Path(__file__).parent / "resources"

def test_typistry():

    tl = TypeLoader(pre_load_modules=["example_types"])
    ty = Typistry(type_loader=tl)
    ti = Tingistry.create(typistry=ty)

    from example_types.example_1 import ExampleType1
    pt_id = ti.register_prototing(prototing_ting_id="examples", prototing_class=ExampleType1, allow_existing=False)

    ting_1 = ti.create_ting(prototing_id=pt_id, ting_id="ting_1")
    print(ting_1)
    print(ting_1.obj)


