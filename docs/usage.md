# Usage


## Getting help

To get information for the `typistry` command, use the ``--help`` flag:

{{ cli("typistry", "--help") }}
