[![PyPI status](https://img.shields.io/pypi/status/frkl.typistry.svg)](https://pypi.python.org/pypi/frkl.typistry/)
[![PyPI version](https://img.shields.io/pypi/v/frkl.typistry.svg)](https://pypi.python.org/pypi/frkl.typistry/)
[![PyPI pyversions](https://img.shields.io/pypi/pyversions/frkl.typistry.svg)](https://pypi.python.org/pypi/frkl.typistry/)
[![Pipeline status](https://gitlab.com/frkl/frkl.typistry/badges/develop/pipeline.svg)](https://gitlab.com/frkl/frkl.typistry/pipelines)
[![coverage report](https://gitlab.com/frkl/frkl.typistry/badges/develop/coverage.svg)](https://gitlab.com/frkl/frkl.typistry/-/commits/develop)
[![Code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

# frkl.typistry

*An opinionated type framework.*


## Description

Documentation still to be done.


## Downloads

### Binaries

  - [Linux](https://s3-eu-west-1.amazonaws.com/dev.dl.frkl.io/linux-gnu/typistry)
  - [Windows](https://s3-eu-west-1.amazonaws.com/dev.dl.frkl.io/windows/typistry.exe)
  - [Mac OS X](https://s3-eu-west-1.amazonaws.com/dev.dl.frkl.io/darwin/typistry)


# Development

## Requirements

- Python (version >=3.6)
- pip, virtualenv
- git
- make
- [direnv](https://direnv.net/) (optional)

### Prepare development environment

If you only want to work on the modules, and not the core *Kiara* codebase, follow the instructions below. Otherwise, please
check the notes on how to setup a *Kiara* development environment under (TODO).

```console
git clone https://gitlab.com/frkl/frkl.typistry.git
cd kiara
python3 -m venv .venv
source .venv/bin/activate
make init
```

If you use [direnv](https://direnv.net/), you can alternatively do:

``` console
git clone https://gitlab.com/frkl/frkl.typistry.git
cd kiara
cp .envrc.disabled .envrc
direnv allow   # if using direnv, otherwise activate virtualenv
make init
```

*Note*: you might want to adjust the Python version in ``.envrc`` (should not be necessary in most cases though)

## ``make`` targets

- ``init``: init development project (install project & dev dependencies into virtualenv, as well as pre-commit git hook)
- ``binary``: create binary for project (will install *pyenv* -- check ``scripts/build-binary`` for details)
- ``flake``: run *flake8* tests
- ``mypy``: run mypy tests
- ``test``: run unit tests
- ``docs``: create static documentation pages
- ``serve-docs``: serve documentation pages (incl. auto-reload)
- ``clean``: clean build directories

For details (and other, minor targets), check the ``Makefile``.


## Running tests

``` console
> make test
# or
> make coverage
```


## Update project template

This project uses [cruft](https://github.com/timothycrosley/cruft) to apply updates to [the base Python project template](https://gitlab.com/frkl/template-python-project) to this repository. Check out it's documentation for more information.

``` console
cruft update
# interactively approve changes, make changes if necessary
git add *
git commit -m "chore: updated project from template"
```


## Copyright & license

Please check the [LICENSE](/LICENSE) file in this repository (it's a short license!).

[Parity Public License 7.0.0](https://licensezero.com/licenses/parity)

[Copyright (c) 2021 frkl OÜ](https://frkl.io)
